﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figury
{
    class Square : IShape
    {
        private float Area;
        private float Circum;
        private int a;
        
        public Square()
        {
            RandomizeShape();
        }
        public Square(Random random)
        {
            RandomizeShape(random);
        }
        public float GetArea()
        {
            return Area;
        }

        public float GetCircum()
        {
            return Circum;
        }

        public IShape RandomizeShape(Random rand)
        {
            a = rand.Next(1, 99);
            return this;
        }

        public IShape RandomizeShape()
        {
            Random rand = new Random();
            a = rand.Next(1, 99);
            return this;
        }

        public float SetArea()
        {
             
            return Area =(float)(a * a);
        }

        public float SetCircum()
        {
            return Circum =(float)(4 * a);
        }

        public bool Validate()
        {
            return true;
        }
        public override string ToString()
        {
            return GetArea() + " " + GetCircum();
        }
    }
}
