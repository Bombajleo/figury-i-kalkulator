﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figury
{
    class Parallelogram : IShape
    {
        private float Area;
        private float Circum;
        private int a, b, h;

        public Parallelogram()
        {
            RandomizeShape();
        }
        public Parallelogram(Random random)
        {
            RandomizeShape(random);
        }
        public float GetArea()
        {
            return Area;
        }

        public float GetCircum()
        {
            return Circum;
        }

        public IShape RandomizeShape(Random rand)
        {
            a = rand.Next(1, 99);
            b = rand.Next(1, 99);
            h = rand.Next(1, 49);

            return this;
        }

        public IShape RandomizeShape()
        {
            Random rand = new Random();
            a = rand.Next(1, 99);
            b = rand.Next(1, 99);
            h = rand.Next(1, 19);

            return this;

        }

        public float SetArea()
        {
            return Area = (float)(b * h);
        }

        public float SetCircum()
        {
            return Circum = (float)(2 * a + 2 * b);
        }

        public bool Validate()
        {
            return true;
        }
        public override string ToString()
        {
            return GetArea() + " " + GetCircum();
        }
    }
}
