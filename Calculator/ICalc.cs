﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    interface ICalc
    {
        float GetScore();
        ICalc RandomizeScore();
        ICalc RandomizeScore(Random rand);
        float SetScore();
        bool Validate();
    }
}
