﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Adding : ICalc
    {
        private float Score;
        private int a, b;

        public Adding()
        {
            RandomizeScore();
        }
        public Adding(Random random) 
        {
            RandomizeScore(random);
        }
        public float GetScore()
        {
            return Score;
        }

        public ICalc RandomizeScore()
        {
            Random rand = new Random();
            a = rand.Next(1, 99);
            b = rand.Next(1, 99);
            return this;
        }

        public ICalc RandomizeScore(Random rand)
        {
            a = rand.Next(1, 99);
            b = rand.Next(1, 99);
            return this;
        }

        public float SetScore()
        {
            Score = a + b;
            return Score;
        }

        public bool Validate()
        {
            return true;
        }
        public override string ToString()
        {
            return SetScore() + " ";
        }
    }
}
