﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            List<ICalc> lista = new List<ICalc>();
            Random rand = new Random();

            for (int i = 0; i < 1; i++)
            {
                lista.Add(new Multiplication(rand));
            }
            for (int i = 0; i < 1; i++)
            {
                lista.Add(new Dividing(rand));
            }
            for (int i = 0; i < 1; i++)
            {
                lista.Add(new Adding(rand));
            }
            for (int i = 0; i < 1; i++)
            {
                lista.Add(new Subtract(rand));
            }
            foreach (var item in lista)
            {
                item.GetScore();
            }

            //lista = lista.OrderByDescending(x => x.GetScore()).ToList();
            foreach (var item in lista)
            {
                Console.WriteLine(item.ToString());
            }
            Console.Read();
        }
    }
}
